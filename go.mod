module gitee.com/vesmr/test-websockethttp-server

go 1.17

require (
	gitee.com/vesmr/websockethttp-go v1.0.3 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
)
