FROM alpine
LABEL authors=Ves.X.Pan
USER root
WORKDIR /root
VOLUME /data
EXPOSE 1001
COPY ./main ./
CMD ./main


